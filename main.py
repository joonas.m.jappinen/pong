import pygame
from pygame.locals import *

class Bat():
  
    def __init__(self, vel, pos):
        self.vel = vel
        self.pos = pos

    def moveup(self):
        self.pos -= self.vel

    def movedown(self):
        self.pos += self.vel
        

class Ball():
    
    def __init__(self, x, y, xvel, yvel):
        self.x = x
        self.y = y
        self.xvel = xvel
        self.yvel = yvel

red = (255, 0, 0)
black = (0, 0, 0)
white = (255, 255, 255)

Ball = Ball(375, 250, 5, 5)

# BATS Bat(velocity, position)
PBat = Bat(10, 200)
AIBat = Bat(10, 200)

# Changes window size
screen = pygame.display.set_mode((750, 500))

# Changes window name to "Pong"
caption = pygame.display.set_caption("Pong")

# Setup for tickrate clock
clock = pygame.time.Clock()

# Starts gameloop
run = True

# Gameloop
while run:

    # Tickrate
    clock.tick(60)

    # Gets key state changes
    keys = pygame.key.get_pressed()

    # Makes closing the game possible
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    # Control UP 
    if keys[pygame.K_w]:
        if PBat.pos > 0:
            PBat.moveup()
        else:
            pass
    
    # Control DOWN
    if keys[pygame.K_s]:
        if PBat.pos < 425:
            PBat.movedown()
        else:
            pass
    
    if Ball.y < 500 and Ball.y > 0:
        Ball.y = Ball.y + Ball.yvel
    else:
        Ball.yvel = Ball.yvel * -1
        Ball.y = Ball.y + Ball.yvel
    
    if Ball.x < 750 and Ball.x > 0:
        Ball.x = Ball.x + Ball.xvel
    else: 
        break

    if Ball.x > 730:
        if Ball.y > AIBat.pos and Ball.y < (AIBat.pos + 75):
            Ball.xvel = Ball.xvel * -1
        elif Ball.y > PBat.pos and Ball.y < (PBat.pos + 75):
            Ball.xvel = Ball.xvel * -1
        elif Ball.y < PBat.pos or Ball.y >  (PBat.pos + 75):
            pass

    if  Ball.x < 20:
        if Ball.y > PBat.pos and Ball.y < (PBat.pos + 75):
            Ball.xvel = Ball.xvel * -1
        elif Ball.y > PBat.pos and Ball.y < (PBat.pos + 75):
            Ball.xvel = Ball.xvel * -1
        elif Ball.y < PBat.pos:
            pass 
        if Ball.y >  (PBat.pos + 75):
            pass
    AIBat.pos = Ball.y - 37
    print(Ball.y)
    # Drawing call
    screen.fill(black)
    pygame.draw.rect(screen, white, (0, PBat.pos, 20, 75)) # Player draw
    pygame.draw.rect(screen, white, (730, AIBat.pos, 20, 75)) # AI draw
    pygame.draw.circle(screen, white, (Ball.x, Ball.y), 5) # Ball draw

    # Updates the screen
    pygame.display.update()

# Quits the window after the gameloop has been stopped
pygame.quit()